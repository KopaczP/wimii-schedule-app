package com.example.kopac.wimiplan.Plan.Models;

import java.io.Serializable;

public class Semester implements Serializable {
    public String TermName;
    public String HyperLink;
    public boolean IsStationary;
}
